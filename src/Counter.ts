export default interface Counter {
  readonly x: number;
  inc(): void;
}
