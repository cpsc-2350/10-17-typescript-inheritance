// export interface PointVals {
//   readonly x: number;
//   readonly y: number;
// }

type XY = {readonly x: number, readonly y: number};
export type PointVals = XY;

export class PointValsImpl implements PointVals{
  protected _x: number; // the keyword protected allows only the classes that inherit to acess and modify these values
  protected _y: number;
  
  constructor(xy: PointVals);
  constructor(x: number, y: number);
  constructor(arg1: number | PointVals, arg2?: any){
    if (typeof arg1 === 'number'){
      this._x = arg1;
      this._y = arg2;
    } else {
      this._x = arg1.x;
      this._y = arg2.y;
      
    }
  }
  
  get x(): number{
    return this._x;
  }

  get y(): number{
    return this._y;
  }
  
}

export interface Point extends PointVals {
 equals(o: PointVals) : boolean;
 distance(o: PointVals): number;
 move(dx: number, dy: number): void;
}

export class PointImpl extends PointValsImpl implements Point{
  equals(o: PointVals): boolean {
    return o.x === this.x && o.y === this.x;  
  }
  distance(o: PointVals): number{
    return Math.sqrt((this.x - o.x)**2 + (this.y - o.y)**2);
  };
  move(dx: number, dy: number): void{
    this._x += dx;
    this._y += dy;
  }
}