import Counter from "./Counter";

export default class CountDown implements Counter {
  private _x: number;

  constructor(initialValue: number) {
    this._x = initialValue;
  }

  get x(): number {
    return this._x;
  }
  inc(): void {
    this._x--;
  }
}
