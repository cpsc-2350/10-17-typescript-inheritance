import { PointImpl } from "./Point";
import Counter from "./Counter";
import SingleCounter from "./SingleCounter";
import DoubleCounter from "./DoubleCounter";
import NCounter from "./NCounter";
import CountDown from "./CountDown";

const pointA = new PointImpl(1, 1);

console.log(pointA);

pointA.move(1, -1);

console.log(pointA);

/*
const single = new SingleCounter();
const double = new DoubleCounter();
const ncount = new NCounter(3);
const countdown = new CountDown(10);
*/

function showIncrement(counter: Counter): void {
  console.log("Starting new increment cycle");
  console.log(counter);
  counter.inc();
  console.log(counter);
  counter.inc();
  console.log(counter);
}

const counters = [
  new SingleCounter(),
  new DoubleCounter(),
  new NCounter(3),
  new CountDown(10),
];

/*
for (let counter of counters) {
  console.log(counter);
  counter.inc();
  console.log(counter);
}
*/
counters.forEach(showIncrement);
