import NCounter from "./NCounter";

export default class DoubleCounter extends NCounter {
  //private _x: number;

  constructor() {
    super(2);
    //this._x = 0;
  }

  /*
  get x(): number {
    return this._x;
  }
  inc(): void {
    this._x += 2;
  }
*/
}
