import Counter from "./Counter";

export default class NCounter implements Counter {
  private _x: number;

  constructor(private readonly delta: number) {
    this._x = 0;
  }

  get x(): number {
    return this._x;
  }
  inc(): void {
    this._x += this.delta;
  }
}
